% this function calculates the confidence interval of individual
% performance
% input data structure: an array of performance coded in 1 (hit) and 0
% (miss)
function ci_perf = ciPerf(data)
n = length(data);
p = sum(data)/length(data);
ci_perf = sqrt(p.*(1 - p)./(n)); 