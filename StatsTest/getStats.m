% input: array of data points
function [avg, ci, std, sem, n] = getStats(data)
avg = nanmean(data);
ci = ciPerf(data);
std = nanstd(data);
sem = nansem(data, 2);
n = length(data);
