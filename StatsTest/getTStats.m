%generates all necessary paired test results input - a
%matrix: 2 columns - conditions, row - subjects
function [results] = getTStats(vec)
[~, results.p, ~, stats] = ttest(vec(:, 1), vec(:, 2));
results.df = stats.df;
results.t = round(stats.tstat, 2);