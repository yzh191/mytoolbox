%generates all necessary anova results including multicomparison, input - a
%matrix: column - conditions, row - subjects
function [results] = getAnovaStats(vec)
[~, tbl, stats] = anova2(vec);
results.df(1) = tbl(2, 3); 
results.df(2) = tbl(4, 3);
results.F = tbl(2, 5); 
results.p_f = tbl(2, 6); % p from anova stats (F)
c = multcompare(stats);
results.p_c = c(:, end)'; % p from multicompare in the order of (cond1 vs. cond2, cond1 vs. cond3, cond2 vs. cond3)