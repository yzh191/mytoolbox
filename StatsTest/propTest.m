% INPUT
% data: a vector of 1s and 0s of hit and misses
% Po: the hypothesis proportion
% OUTPUT
% p: probability of sample proportion is the same as hypothesis propotion
% ci: confidence interval of sample proportion
function [p, ci] = propTest(data, Po) % two sided proportion test
n = length(data);
Ps = sum(data)/length(data);
z = (Ps - Po)/sqrt((Po*(1 - Po)/n));
p = normpdf(z);
ci = -norminv(0.05/2)*sqrt(Ps * (1 - Ps) / n);