% x is an input array of variables in smoothed windows
% y is the performance being averaged inside the windows
% window is the smoothed windows calculated from getSlideWindow
function [smooth_y, ci_y, n] = getSlidePerf(x, y, window)
% calculate smoothed performance
smooth_y = zeros(1, length(window));
ci_y = zeros(1, length(window));
n = zeros(1, length(window));
for i = 1:length(window)
    smooth_y(i) = ...
        mean(y(isBetween(window(i, 1), x, window(i, 2))));
    ci_y(i) = ...
        ciPerf(y(isBetween(window(i, 1), x, window(i, 2))));
    n(i) = ...
        sum(isBetween(window(i, 1), x, window(i, 2)));
end