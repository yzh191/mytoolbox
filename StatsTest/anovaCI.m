% this function calculates the confidence interval of an anova test across
% conditions. Input - matrix of data (column: conditions, row: subject)

function [CI] = anovaCI(varargin)
mat = varargin{1};
%remove nans
mat(all(isnan(mat),2),:) = [];
try 
    alpha = varargin{2};
catch 
    alpha = 0.05;
end
nCond = size(mat, 2);
nSubj = size(mat, 1);

for ii = 1:nSubj
    MAcrossCond(ii) = mean(mat(ii,:));
end

for ii = 1:nCond
    MAcrossSubj(ii) = mean(mat(:,ii));
end
MTot = mean(MAcrossCond);

%represents the variation within a condition

for ii = 1: nSubj
    SSWin(ii) = nCond.* ((MAcrossCond(ii) - MTot).^2);
end
SSWin = sum(SSWin);

%represents the variation due to the condition

for ii = 1:nCond
    SSBtw(ii) = nSubj.* ((MAcrossSubj(ii) - MTot).^2);
end
SSBtw = sum(SSBtw);

%Sum of squares total; represents total variation

for ii = 1:nSubj
    for jj = 1:nCond
        SST(ii,jj) = ((((mat(ii,jj) - MTot).^2)));
    end
end
SST = sum(sum(SST));

%sum of square error; represents the variation due to the error

SSe = SST - SSBtw - SSWin;

%F value computation

dfBtw = nCond - 1;
dfWin = nSubj - 1;
dfe = dfBtw * dfWin;
MSE = SSe/dfe;

%Determine CI

CI = abs((sqrt(MSE/nSubj))*tinv(alpha/2,dfe));
