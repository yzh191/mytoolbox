% out - logical of whether the value is between the lower and upper
% intervals
% out = isBetween(lowLimit, value being tested (can be an array), upper
% limit)
function out = isBetween(lowerLimit, middleValue, upperLimit)
out = (middleValue >= lowerLimit) & (middleValue < upperLimit);
out = logical(out);