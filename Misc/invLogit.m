function [out] = invLogit(x)
out = exp(x)/(1+exp(x));
