MONITOR_DISTANCE = 2387; %mm 
PIXEL_X = 2560; %1920
PIXEL_Y = 1440; %1080
DisplayWidth = 585; %mm 543 
DisplayHeight = 330; % 306
pxAngX = atan(DisplayWidth/2/MONITOR_DISTANCE)/pi*180*60/(PIXEL_X/2); % arcmin of each pixel
pxAngY = atan(DisplayHeight/2/MONITOR_DISTANCE)/pi*180*60/(PIXEL_Y/2); % arcmin of each pixel