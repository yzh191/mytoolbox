%scale stimulus based on cortical magnification: all in arcmin
function [newSize] = scaleSizeCM(baseSize, baseEcc, newEcc)
% calculate factor of the base target
if baseEcc > 60
    supBase = 7.99*(1.0 + 0.42 * baseEcc/60 + 0.00012 * (baseEcc/60)^3)^(-1)/60;
    infBase = 7.99*(1.0 + 0.42 * baseEcc/60 + 0.000055 * (baseEcc/60)^3)^(-1)/60;
    baseM = mean([supBase infBase]);
elseif baseEcc <= 60
    baseM = 7.99/60;
end

% calculate factor of the new target
if newEcc > 60
    supNew = 7.99*(1.0 + 0.42 * newEcc/60 + 0.00012 * (newEcc/60)^3)^(-1)/60;
    infNew = 7.99*(1.0 + 0.42 * newEcc/60 + 0.000055 * (newEcc/60)^3)^(-1)/60;
    newM = (supNew + infNew) / 2;
elseif newEcc <= 60
    newM = 7.99/60;
end
newSize = baseSize * baseM/newM;

