function finalTableOutput(finalTable, tableParams, filepath, latexFilename)

% finalTable: the table that you want generated
% filepath: the destination of the txt file
% latexFilename: the txt file name


% Now use this table as input in our input struct:
input.data = finalTable;

% Set the row format of the data values, 
% One format type: {'%i'}
% More than one type: {'%.3f', 2, '%.1f', 1}
input.dataFormat = tableParams.dataFormat;
input.dataFormatMode = 'column';

% Column alignment ('l'=left-justified, 'c'=centered,'r'=right-justified):
input.tableColumnAlignment = 'c';

% Switch table borders on(1) / off (0):
input.tableBorders = 1;

% Switch to generate a complete LaTex document or just a table:
% i.e. writes the begin document, end document, etc.
input.makeCompleteLatexDocument = 0;

% Switch to include begin/end table:
input.makeCompleteTableDocument = tableParams.completeTable;

% LaTex table caption:
input.tableCaption = tableParams.tableCaption;

% LaTex table label:
input.tableLabel = tableParams.tableLabel;

% Switch transposing/pivoting your table:
input.transposeTable = tableParams.transposeTable;
 
% Positioning of the table
input.tablePlacement = 'H';

% Checks if the text file exists, if it does, it deletes it and then makes
% a new one
if (exist(sprintf('%s/%s', filepath, latexFilename), 'file') == 2)
    delete(sprintf('%s/%s', filepath, latexFilename))
end

% Writes the table into a txt file, it MUST be a txt file for Latex to read
% it properly
diary(sprintf('%s/%s', filepath, latexFilename))
diary ON
latexTable(input);
diary OFF

end

