% response = subject response;
% target = target;
% target_array = [1 2] or [0 1]
function [d std_d ci_d crit var_d] = dprime(response, target, target_array)

Hi = length( find(target == target_array(2) & response == target_array(2)) );
Fa = length( find(target == target_array(1) & response == target_array(2)) );
Mi = length( find(target == target_array(2) & response == target_array(1)) );
Cr = length( find(target == target_array(1) & response == target_array(1)) );


 
HitRate = Hi/(Hi+Mi);
FaRate = Fa/(Cr+Fa);


% apply correction to have bounds to dprime
[HitRate FaRate] = CorrectionDPrime(HitRate, FaRate, Hi, Mi, Fa, Cr);

d = norminv(HitRate)-norminv(FaRate);

% % percent 
% PercentMiss_i = Mi/(Mi+Hi+Fa+Cr);
% PercentFA_i = Fa/(Mi+Hi+Fa+Cr);
% PercentMiss_v = Mv/(Mv+Hv+Fa+Cr);
% PercentFA_v = Fa/(Mv+Hv+Fa+Cr);
% PercentMiss_n = Mn/(Mn+Hn+Fa_n+Cr_n);
% PercentFA_n = Fa_n/(Mn+Hn+Fa_n+Cr_n);

% standard deviation
% from Mcmillian and Creelman - detection theory pg 325-327
phi_h = (2 * pi)^-.50 * exp(-.5 * ((sqrt(2) * erfinv(2 * HitRate - 1))^2));
phi_f = (2 * pi)^-.50 * exp(-.5 * ((sqrt(2) * erfinv(2 * FaRate - 1))^2));
var_d = (((HitRate * (1 - HitRate))/((Hi+Mi) * ((phi_h)^2))) + ...
    ((FaRate * (1 - FaRate))/((Cr+Fa) * ((phi_f)^2))));
std_d = sqrt(var_d);
ci_d = std_d * 1.96;
%-------------------------------

% bias/criterion
crit = -([norminv(HitRate)+norminv(FaRate)])/2;

function [HitRate FaRate] = CorrectionDPrime(HitRate, FaRate, Hi, Mi, Fa, Cr)
% correction approach see stanislaw and Todorow 99

if (HitRate == 0)
    %HitRate = (1/(2*(H+M)));
    HitRate = 0.5/(Hi+Mi);
end
if (FaRate == 0)
    %FalseAlarmRate = (1/(2*(F+C)));
    FaRate = 0.5/(Fa+Cr);
end
if (HitRate == 1)
    %HitRate = (1-(1/(2*(H+M))));
    HitRate = 1-(0.5/(Hi+Mi));
end
if (FaRate == 1)
    %FalseAlarmRate = (1-(1/(2*(F+C))));
    FaRate = 1-(0.5/(Fa+Cr));
end

