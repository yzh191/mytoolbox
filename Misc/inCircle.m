% check whether the point is in the central circle
function out = inCircle(px, py, r)
% calculate start and end points of rectangles
% r = radius of the cicle
out = zeros(1, length(px));
% out = 1 if the point is within the region
for i = 1:length(px)
    dist = sqrt(px(i)^2 + py(i)^2);
    out(i) = dist < r;
end