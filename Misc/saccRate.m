function [ratePerS] = saccRate(numEvents, dur)

if ~isempty(numEvents)
    ratePerMs = numEvents / dur;
    ratePerS = ratePerMs * 1000;
else
    ratePerS = 0;
end
end

