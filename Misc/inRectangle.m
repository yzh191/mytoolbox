function out = inRectangle(px, py, rect)
% calculate start and end points of rectangles
% rect = [topleft x, topleft y, width, height]
out = zeros(1, length(px));
sx = rect(1);
ex = sx + rect(3);
sy = rect(2);
ey = sy + rect(4);

% out = 1 if the point is within the region
for i = 1:length(px)
    out(i) = (sx < px(i)) && (px(i) < ex) && (sy < py(i)) && (py(i) < ey);
end