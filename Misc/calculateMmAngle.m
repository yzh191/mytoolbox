% inputs:
% size_mm = target size on the screen in mm
% view_distance_mm = viewing distance between observer and the screen in mm
% output:
% size_arcmin = target size in arcminutes based on viewing distance
function size_arcmin = calculateMmAngle(size_mm, view_distance_mm)
size_arcmin = 2*atand(size_mm/2/view_distance_mm)*60;
