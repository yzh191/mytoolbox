%% Filter traces using butterworth
% n: Filter order (1)
% fc: cutoff frequency (15Hz)
% fs: sampling frequency (1kHz)
%%
function filtered = smTrace(x, n, fc, fs)
    [b, a] = butter(n, fc/(fs/2));
    filtered = filtfilt(b, a, x);
end