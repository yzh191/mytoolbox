function [saccInfo, driftInfo, issaccade, isdrift, speed] = mySegmentTrace(x, y, isblinknotrack, Fs)
% rewrite especially for DDPI data

if Fs > 990
    winsize = 51;   % for ddpi-mk2, use a larger window becuase of 85Hz oscillation, for saccade
    smoothing = 51;
elseif Fs >=400 && Fs <= 600
    winsize = 27;
    smoothing = 27;
elseif Fs < 400
    winsize = 15;
end

% clean up the bad traces to interfere less with speed calculation
if sum(~isblinknotrack) > 10
    x(isblinknotrack) = interp1(find(~isblinknotrack), x(~isblinknotrack), find(isblinknotrack), 'linear', 'extrap');
    y(isblinknotrack) = interp1(find(~isblinknotrack), y(~isblinknotrack), find(isblinknotrack), 'linear', 'extrap');
else
    x = nan(size(x));
    y = nan(size(y));
end

if mean(isblinknotrack) > .5
    saccInfo = [];
    issaccade = false(size(x));
    driftInfo = [];
    isdrift = false(size(x));
    speed = nan(size(x));
    return;
end

% detect saccades, this list incorporates "jerk" movements which are high
% speed movements that seem to go nowhere
[saccInfo, issaccade, speed] = detectSaccades(x, y, isblinknotrack, Fs, winsize);

isdrift = ~isblinknotrack & ~issaccade;
[driftInfo, isdrift] = analyzeDrift(x, y, isdrift, Fs, smoothing);

end


function [saccInfo, issaccade, speed] = detectSaccades(xx, yy, isblinknotrack, Fs, winsize)

minSaccadeSpeed = 3;
startVel = 2;

minPeakWidth = (10 / 1000) * Fs;
maxPeakWidth = (100 / 1000) * Fs;
minPeakProminence = minSaccadeSpeed;

minGap = round((15 / 1000) * Fs);
minDur = round((10 / 1000) * Fs);
maxDur = round((300/ 1000) * Fs);
minAmp = 3; % arcmin


velx = sgfilt(xx, 3, winsize, 1);
vely = sgfilt(yy, 3, winsize, 1);
speed = sqrt(velx.^2 + vely.^2) * Fs / 60;

speed(isblinknotrack) = nan;
% turn off warming
warning('off', 'signal:findpeaks:largeMinPeakHeight')
[pks, locs, widths, proms] = findpeaks(speed, ...
    'MinPeakHeight', minSaccadeSpeed,...  % 3 deg/s
    'MinPeakWidth', minPeakWidth,...
    'MinPeakProminence', minPeakProminence,...
    'MaxPeakWidth', maxPeakWidth);
% figure(50); clf;
% findpeaks(speed, ... % for plotting
%     'MinPeakHeight', minSaccadeSpeed,...  % 3 deg/s
%     'MinPeakWidth', minPeakWidth,...
%     'MinPeakProminence', minPeakProminence,...
%     'MaxPeakWidth', maxPeakWidth,...
%     'Annotate','extents');
[proms, idx] = sort(proms, 'descend');
locs = locs(idx);
widths = widths(idx);
pks = pks(idx);

% remove those peaks in which only sample exceeds threshold - this is now
% handled by the prominence parameter

kill = false(size(pks));
for si = 1:length(pks)
    ti = locs(si);
    if ti > 1
        kill(si) = speed(ti-1) < minSaccadeSpeed; 
    end
    if ti < length(speed)
        kill(si) = speed(ti+1) < minSaccadeSpeed; 
    end
end

%kill = false(size(pks))
pks(kill) = [];
locs(kill) = [];
proms(kill) = [];
widths(kill) = [];

issaccade = false(size(xx));
for si = 1:length(pks)
    ti = locs(si);
    if issaccade(ti) || isblinknotrack(ti) % already a saccade or tagged blink or no-track
        continue; 
    end
    
    % we found a new saccade so find its start and end
    % set a dynamic threshold for start and stop
%     startVel_tmp = max([startVel, ...
%         pks(si) - proms(si) + startVel...
%         pks(si) - 0.8 * proms(si)]);
    startVel_tmp = startVel;
    
    ss = find(speed(1:(ti-1)) < startVel_tmp, 1, 'last') + 1;
    ee = ti + find(speed((ti+1):end) < startVel_tmp, 1, 'first') - 1;
    
    if (ee - ss) > maxDur
        continue; 
    end
    
    if ti < 100 && isempty(ss)
        ss = 1; 
    end
    if ti > length(xx) - 100 && isempty(ee)
        ee = length(xx); 
    end
    
    issaccade(ss:ee) = true;
end

[startIndex, stopIndex] = getIndicesFromBin(issaccade);

%%%%%%%% merge saccades that are too close together
for si = 1:(length(startIndex) - 1)
    stop = stopIndex(si);
    startnext = startIndex(si+1);
    
    if abs(startnext - stop) < minGap
        % merge two saccades together
        startIndex(si+1) = startIndex(si);
        stopIndex(si) = stopIndex(si+1);
    end
end
startIndex2 = startIndex;
stopIndex2 = stopIndex;
startIndex = unique(startIndex2);
stopIndex = nan(size(startIndex));
for si = 1:length(startIndex)
    si_use =  (startIndex2 == startIndex(si));
    stopIndex(si) = max(stopIndex2(si_use));
end

kill = (stopIndex - startIndex) <= minDur; % kill saccades with short duration
startIndex(kill) = [];
stopIndex(kill) = [];

%%%%%% compute amplitude and angle, discard those that are too small
amp = sqrt((xx(startIndex) - xx(stopIndex)).^2 + (yy(startIndex) - yy(stopIndex)).^2);
ang = atan2(yy(startIndex) - yy(stopIndex), xx(startIndex) - xx(stopIndex));
jerk = amp(:) < minAmp; % possibly a jerk????
% startIndex(jerk) = [];
% stopIndex(jerk) = [];
% amp(jerk) = [];
% ang(jerk) = [];

%%%%%% save into structure
if ~isempty(amp)
    saccInfo(length(amp)) = struct();
    issaccade = false(size(xx));
    for si = 1:length(amp)
        saccInfo(si).startIndex = startIndex(si);
        saccInfo(si).stopIndex = stopIndex(si);
        saccInfo(si).amplitude = amp(si);
        saccInfo(si).angle = ang(si);
        saccInfo(si).duration = (stopIndex(si) - startIndex(si)) / Fs * 1000;
        saccInfo(si).pkspeed = max(speed(startIndex(si):stopIndex(si)));
        
        issaccade(startIndex(si):stopIndex(si)) = true;
    end
else
    saccInfo = struct(...
        'startIndex', [], 'stopIndex', [], ...
        'amplitude', [], 'angle', [], 'duration', [], 'pkspeed', []);
end



if false % debug plotting
    t = (1:length(yy)) / Fs;
    figure(100); clf;
    ax(1) = subplot(2, 1, 1); hold on;
    plot(t, xx - mean(xx), 'b.'); plot(t, yy - mean(yy), 'r.');
    yl = ylim;
    yl = [-50, 50];
    for si = 1:length(startIndex)
        h = patch(t([startIndex(si), startIndex(si), stopIndex(si), stopIndex(si)]),...
            yl([1, 2, 2, 1]), 'r', 'FaceAlpha', .3, 'EdgeColor', 'none');
    end
    grid on;
    
    ax(2) = subplot(2, 1, 2); hold on;
    plot(t, speed, 'k.');
    yl = ylim;
    for si = 1:length(startIndex)
        h = patch(t([startIndex(si), startIndex(si), stopIndex(si), stopIndex(si)]),...
            yl([1, 2, 2, 1]), 'r', 'FaceAlpha', .3, 'EdgeColor', 'none');
    end
    grid on;
    ylim([0, 50]);
    linkaxes(ax, 'x');
end

end

function [driftInfo, isdrift] = analyzeDrift(xx, yy, isdrift, Fs, smoothing)
[startIndex, stopIndex] = getIndicesFromBin(isdrift);

cutseg = floor(smoothing / 2);
maxSpeed = inf;

minDuration = 50; %ms

driftInfo(length(startIndex)) = struct();
isdrift = false(size(isdrift));
kill = false(size(startIndex));
for si = 1:length(startIndex)
    ss = startIndex(si);
    ee = stopIndex(si);
    
    if (ee - ss) / Fs * 1000 < minDuration
        kill(si) = true;
        continue; 
    end
    
    isdrift(ss:ee) = true;
    
    [span, mn_speed, mn_cur, varx, vary, smx, smy, smx1, smy1] = ...
        getDriftChar(xx(ss:ee), yy(ss:ee), smoothing, cutseg, maxSpeed, Fs);
    
    driftInfo(si).startIndex = ss;
    driftInfo(si).stopIndex = ee;
    driftInfo(si).x_raw = xx(ss:ee);
    driftInfo(si).y_raw = yy(ss:ee);
    driftInfo(si).x = smx;
    driftInfo(si).y = smy;
    driftInfo(si).xvel = smx1;
    driftInfo(si).yvel = smy1;
    
    driftInfo(si).span = span;
    driftInfo(si).speed = mn_speed;
    driftInfo(si).curv = mn_cur;
    driftInfo(si).varx = varx;
    driftInfo(si).vary = vary;
    driftInfo(si).duration = (ee - ss) / Fs * 1000;
end
driftInfo(kill) = [];
end
