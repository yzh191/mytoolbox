function [startIdx, stopIdx] = getIndicesFromBin(bin)
bin = bin(:);

if ~any(bin)
    startIdx = [];
    stopIdx = [];
    return;
end

if all(bin)
    startIdx = 1;
    stopIdx = length(bin);
    return;
end

df = diff(bin);
startIdx = find(df > 0) + 1;
stopIdx = find(df < 0);

if isempty(stopIdx) && bin(end)
    stopIdx = length(bin); 
end
if isempty(startIdx) && bin(1)
    startIdx = 1; 
end

if (isempty(startIdx) && ~isempty(stopIdx)) || (startIdx(1) > stopIdx(1))
    startIdx = [1; startIdx];
end
if stopIdx(end) < startIdx(end)
    stopIdx = [stopIdx; length(bin)]; 
end
end