function [span, mn_speed, mn_cur, varx, vary, smx, smy, smx1, smy1] = getDriftChar(x, y, smoothing, cutseg, maxSpeed, Fs)

% compute drift span
mx = mean(x(cutseg:end-cutseg));
my = mean(y(cutseg:end-cutseg));
span = max(sqrt((x - mx).^2 + (y - my).^2));

% s-golay filtering
smx = sgfilt(x, 3, smoothing, 0);
smy = sgfilt(y, 3, smoothing, 0);
smx1 = sgfilt(x, 3, smoothing, 1);
smy1 = sgfilt(y, 3, smoothing, 1);

% compute drift speed
sp = Fs*sqrt(smx1(cutseg:end-cutseg).^2 + smy1(cutseg:end-cutseg).^2);

if any(sp > maxSpeed)
    mn_speed = nan;
else
    mn_speed = mean(sp);
end


% compute drift curvature
tmpCur = abs(cur(smx(cutseg:end-cutseg), smy(cutseg:end-cutseg)));
mn_cur = mean(tmpCur(tmpCur < 300));
%mn_cur = nan;

varx = var(x(cutseg:end-cutseg));
vary = var(y(cutseg:end-cutseg));
end

function curvatur=cur(x,y)
    dx = gradient(x);
    ddx = gradient(dx);
    dy = gradient(y);
    ddy = gradient(dy);

    num = dx .* ddy - ddx .* dy;
    denom = dx .* dx + dy .* dy;
    denom = sqrt(denom);
    denom = denom .* denom .* denom;
    curvatur = num ./ denom;
    curvatur(denom < 0) = NaN;
end
