function [patch, mask] = Gabor( wavelength, orientation, phase, amplitude, width, height, window, sigma )
	%% patch = Gabor( waveLength, orientation, pahse, width, height, window, sigma )
    %  waveLength:      length of each cycle in pixels
    %  orientation:		counterclockwise; vertical gabor at 0; degrees
	%  pahse:			degrees
	%  width:			width of the patch in pixels (2nd dimension / x)
	%  height:			height of the patch in pixels (1st dimension / y)
	%  window:			'gaussian' (gabor), 'circle' (a round patch) or 'grating' (grating)
	%  sigma:			sigma of the Gaussian filter for 'gaussian' window, or of the Gaussian edge for 'circle' window (set to <=0 for a hard dented edge)
	%  patch:			1st dimension: vertical(y); 2nd dimension: horizontal(x)
	if( nargin() < 7 )
		window = 'grating';
	elseif( nargin() < 8 )
		sigma = 1;
	end
	
	[ x, y ] = meshgrid( (1:width) - (1+width)/2.0, (1:height) - (1+height)/2.0 );
	X = x.*cosd(orientation) + y.*sind(orientation);
	Y = y.*cosd(orientation) - x.*sind(orientation);
	frequency = 1/wavelength;
	if( strcmpi( window, 'gaussian' ) )
        patch = amplitude * cos( 2 * pi * frequency .* X + phase/180*pi ) .* exp( -0.5 * ((x/width*height).^2+y.^2) / sigma^2 );
	else
		patch = amplitude * cos( 2 * pi * frequency .* X + phase/180*pi );
	end
	if( strcmpi( window, 'circle' ) )
		mask = ones(height, width);
		if( sigma <= 0 )
			mask( ((x/width).^2+(y/height).^2) > 1/4 ) = 0;
		else
			index = (x/width*height).^2 + y.^2 >= (height/2-2*sigma)^2;			% make an edge of 2*sigma in vertical and 2*sigma/height*width in horizontal
			mask(index) = normpdf( sqrt( (x(index)/width*height).^2 + y(index).^2 ), height/2-2*sigma, sigma ) / normpdf(0,0,sigma);
		end
		patch = patch .* mask;
    else
        mask = [];
	end
	% patch = patch / max(abs(patch(:)));		% remove this normalization so that its power does not vary with phase
end