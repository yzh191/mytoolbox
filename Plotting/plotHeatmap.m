function plotHeatmap(result, limit)

% figure

cm = heat(length(result));
set(gcf, 'Colormap', cm)
hold on
pcolor(linspace(-limit, limit, size(result, 1)), linspace(-limit, limit, size(result, 1)), result');
% imgaussfilt(f)
plot([-limit limit], [0 0], '--k')
plot([0 0], [-limit limit], '--k')

set(gca, 'FontSize', 15, 'xtick', -limit:20:limit, 'ytick',  -limit:20:limit, 'layer', 'top', 'LineWidth', 2)
box on
xlabel('X (arcmin)')
ylabel('Y (arcmin)')
axis tight
axis square
caxis([floor(min(min(result))) ceil(max(max(result)))])
colorbar('northoutside')
shading interp;

% clear figure

