function plotYAxis(varName)
switch varName
    case 'pc'
        ylabel("Proportion Correct");
        ylim([0.6 1])
        yticks(0.6:0.1:1)
    case 'delta_pc'
        ylabel("Diff(Proportion Correct)");
        ylim([0.6 1])
        yticks(0.6:0.1:1)
    case 'd'
        ylabel("Sensitivity (d')");
        ylim([0 4])
        yticks(0:1:4)
    case 'rt'
        ylabel("Reaction Time (ms)");
end
end