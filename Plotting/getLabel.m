function label = getLabel(varName)
if strcmp('d', varName)
    label = "Sensitivity (d')";
elseif strcmp('pc', varName) || strcmp('perf', varName)
    label = "Proportion correct (%)";
elseif strcmp('rt', varName)
    label = "Reaction time (ms)";
end
