% 9 pastel colors, m = the number of colors needed
function map = pair9(m) 
if nargin == 1
    if m > 9
        warning('There are only 9 colors in this color map')
    else
        values = [
            166,206,227
            31,120,180
            178,223,138
            51,160,44
            251,154,153
            227,26,28
            253,191,111
            255,127,0
            202,178,214
            ]/255;
        map = values((1:m), :);
    end
elseif nargin > 1
    warning('Too many input arguments')
elseif nargin < 1
    warning('Speicify how many colors')
end
