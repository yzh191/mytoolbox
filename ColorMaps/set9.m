% 9 pastel colors, m = the number of colors needed
function map = set9(m)
if nargin == 1
    if m > 9
        warning('There are only 9 colors in this color map')
    else
        values = [
            141,211,199
            237,222,54
            190,186,218
            251,128,114
            128,177,211
            253,180,98
            179,222,105
            252,205,229
            217,217,217
            ]/255;
        map = values((1:m), :);
    end
elseif nargin > 1
    warning('Too many input arguments')
elseif nargin < 1
    warning('Speicify how many colors')
end
