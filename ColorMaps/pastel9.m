% 9 pastel colors, m = the number of colors needed
function map = pastel9(m) 
if nargin == 1
    if m > 9
        warning('There are only 9 colors in this color map')
    else
        values = [
            231   160   154
            159   185   207
            184   215   177
            222   203   228
            254   217   166
            255   255   204
            229   216   189
            253   218   236
            242   242   242
            ]/255;
        map = values((1:m), :);
    end
elseif nargin > 1
    warning('Too many input arguments')
elseif nargin < 1
    warning('Speicify how many colors')
end
