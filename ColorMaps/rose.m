% 9 pastel colors, m = the number of colors needed
function map = rose(m)
if nargin == 1
%     if m > 9
%         warning('There are only 9 colors in this color map')
%     else
        values = [
            255 247 243
            253 224 221
            252 197 192
            250 159 181
            247 104 161
            221 52 151
            174 1 126
            122 1 119
            73 0 106
            ]/255;
        P = size(values,1);
        map = interp1(1:size(values,1), values, linspace(1,P,m), 'linear');
%         %map = values((1:m), :);
%     end
elseif nargin > 1
    warning('Too many input arguments')
elseif nargin < 1
    warning('Speicify how many colors')
end
