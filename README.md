This toolbox contains some useful functions can be used across projects.
* StatsTest: statistical tests including z-test, t-test and anova
* ColorMaps: when you get tired of the MATLAB built-in colormaps
* Misc: just some other random functions
